# coding: utf-8
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template.context import RequestContext
from django.template.context_processors import csrf
from logines.forms import LoginForm, RegForm


def register(request):
    form = RegForm(request.POST or None)
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        email = request.POST.get('email', '')
        if User.objects.filter(username=username):
            return render(request, 'register.html', {'errors': 'Имя уже занято'})

        user = User.objects.create_user(username=username, password=password, email=email)
        if user:
            print('iii:', username)
            print('iii:', password)
            user = authenticate(username=username, password=password, email=email)
            auth.login(request, user)
            return HttpResponseRedirect('/')
        return render(request, str(user), {'form': form})
    else:
        return render(request, 'register.html', {'form': form})


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST or None)
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            print('iii:', username)
            print('iii:', password)
            # Correct password? and the user is marked "active"
            auth.login(request, user)
            # Redirect to a success page.

            return HttpResponseRedirect("/", {'form': form})
        else:
            return render(request, 'login.html', {'errors': 'Ошибка'})
    else:
        form = LoginForm()
        return render(request, 'login.html', {'form': form})


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/")
