__author__ = 'victor'
# coding: utf-8
from django.contrib.auth import authenticate
from django.forms import ModelForm
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label=u'Имя')
    password = forms.CharField(label=u'Пароль', widget=forms.PasswordInput())


class RegForm(forms.Form):
    username = forms.CharField(label=u'Имя')
    password = forms.CharField(label=u'Пароль', widget=forms.PasswordInput())
    email = forms.EmailField(label=u"E-mail")
