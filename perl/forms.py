# coding: utf-8
from django.contrib.auth import authenticate
from django.forms import ModelForm
from models import Comments
from django import forms

class CommentsForm(ModelForm):
    class Meta:
        model = Comments
        fields = ['author_comments', 'mail_author',
                  'text_comments']


# class LoginForm(forms.Form):
#     username = forms.CharField(label=u'Имя')
#     password = forms.CharField(label=u'Пароль', widget=forms.PasswordInput())
#
#     def clean(self):
#         cleaned_data = super(LoginForm, self).clean()
#         if not self.errors:
#             user = authenticate(username=cleaned_data['username'], password=cleaned_data
#             ['password'])
#             if user is None:
#                 raise forms.ValidationError(u'Ошибка ввода')
#             self.user = user
#         return cleaned_data
#
#     def get_user(self):
#         return self.user or None