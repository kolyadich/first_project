# coding: utf-8
# from django.core.context_processors import media
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class Latest_images(models.Model):
    name = models.CharField(max_length=50)
    picture = models.ImageField(upload_to='small_img/', blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Картинка"
        verbose_name_plural = "Картинки"


class Latest_Projects(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=50)
    main_image = models.ImageField(upload_to='big_img/', null=True, blank=True)
    images = models.ManyToManyField('Latest_images', null=True, blank=True)
    small_description = models.CharField(max_length=200)
    description = models.TextField()
    primary = models.BooleanField(default='primary')
    date_created = models.DateTimeField()
    date_updated = models.DateField()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"


class WorkExperience(models.Model):
    title_work = models.CharField(max_length=250)
    company_name = models.CharField(max_length=250)
    date_work = models.DateField()
    date_end_work = models.DateField()
    url_work = models.URLField(max_length=50)
    description_work = models.TextField()

    def __unicode__(self):
        return self.company_name

    class Meta:
        verbose_name = "Работа"
        verbose_name_plural = "Работа"


level_skills = (
    ("basic", ("basic")),
    ("middle", ("middle")),
    ("expert", ("expert")),
)


class SkIlls(models.Model):
    name_language = models.CharField(max_length=100)
    level = models.CharField(choices=level_skills, max_length=70)
    progress = models.IntegerField(
        default=1,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(1)
        ]
    )

    def __unicode__(self):
        return self.name_language

    class Meta:
        verbose_name = "Навык"
        verbose_name_plural = "Навыки"


class TestImonials(models.Model):
    response = models.CharField(max_length=250)
    author_response = models.CharField(max_length=50)
    company_response = models.CharField(max_length=50)

    def __unicode__(self):
        return self.author_response

    class Meta:
        verbose_name = "Рекомендация"
        verbose_name_plural = "Рекомендации"


class LanGuages(models.Model):
    language = models.CharField(max_length=100)
    level = models.CharField(choices=level_skills, max_length=70)
    lang_progress = models.IntegerField(
        default=1,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(1)
        ]
    )

    def __unicode__(self):
        return self.language

    class Meta:
        verbose_name = "Знание языка"
        verbose_name_plural = "Знание языков"


class CategoryPost(models.Model):
    name_category = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name_category


class PostImages(models.Model):
    name_post_images = models.CharField(max_length=50)
    many_img = models.ImageField(upload_to='many_image_post/')

    def __unicode__(self):
        return self.name_post_images


class PoSt(models.Model):
    title_post = models.CharField(max_length=200)
    small_description_post = models.CharField(max_length=250)
    description_post = models.TextField()
    preview_image = models.ImageField(upload_to='post_preview_image/', null=True, blank=True)
    date_created_post = models.DateField()
    date_changed_post = models.DateTimeField()
    category = models.ForeignKey(CategoryPost)
    many_image_post = models.ManyToManyField('PostImages', null=True, blank=True)
    author = models.ForeignKey(User)
    comments_primary = models.BooleanField(default=True)


    def __unicode__(self):
        return self.title_post


class Comments(models.Model):
    author_comments = models.ForeignKey(User)
    post=models.ForeignKey(PoSt)
    mail_author = models.EmailField(max_length=50, null=True, blank=True)
    text_comments = models.TextField()
    date_comments = models.DateTimeField()

    def __unicode__(self):
        return unicode(self.mail_author)



