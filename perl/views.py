# coding: utf-8
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, render, get_object_or_404, redirect
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.template import RequestContext
from django.template.context_processors import csrf
from perl.forms import CommentsForm
from perl.models import Latest_Projects, WorkExperience, SkIlls, TestImonials, LanGuages, PoSt, Comments
from logines.forms import LoginForm




# Create your views here.

def ind(request):
    form = LoginForm
    projects = Latest_Projects.objects.all().order_by('date_created')
    date_projects = projects.exclude(main_image='')
    other_projects = projects.filter(main_image='')
    work = WorkExperience.objects.all()
    work_date = work.order_by('date_work')
    skill = SkIlls.objects.all().order_by('progress')
    testimonials = TestImonials.objects.all().order_by('response')
    languages = LanGuages.objects.all().order_by('lang_progress')

    return render_to_response('index.html', {'date_projects': date_projects, 'other_projects': other_projects,
                                             'work_date': work_date, 'skill': skill, 'testimonials': testimonials,
                                             'languages': languages, 'form': form})


def view_latest_project(request, slug):
    slug_project = Latest_Projects.objects.filter(slug=slug)
    skill = SkIlls.objects.all().order_by('progress')
    testimonials = TestImonials.objects.all().order_by('response')
    languages = LanGuages.objects.all().order_by('lang_progress')
    return render_to_response('view_project.html',
                              {'slug_project': slug_project, 'skill': skill, 'testimonials': testimonials,
                               'languages': languages})



def blog_paginator(request):
    '''Show all post'''
    posts_list = PoSt.objects.all().order_by('-date_created_post')
    paginator = Paginator(posts_list, 2)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    var = dict(
        posts=posts,
    )
    return render_to_response('blog.html', var, context_instance=RequestContext(request))


def one_post(request, post_id):
    one_post = PoSt.objects.filter(id=post_id)
    skill = SkIlls.objects.all().order_by('progress')
    form = CommentsForm
    testimonials = TestImonials.objects.all().order_by('response')
    all_comments = Comments.objects.filter(post_id=post_id)
    languages = LanGuages.objects.all().order_by('lang_progress')
    return render_to_response('post.html', {'one_post': one_post, 'skill': skill, 'testimonials': testimonials,
                                            'languages': languages, 'form': form, 'all_comments': all_comments})


# def add_comment(request, post_id):
#     form = CommentsForm(request.POST or None)
#     if request.method == 'POST':
#         blog_post = PoSt.objects.get(pk=post_id)
#         author_comments = request.POST.get('author_comments')
#         text_comments = request.POST.get('text_comments')
#         mail_author = request.POST.get('mail_author')
#         blog_post.comment_set.create(author_comments=author_comments, text_comments=text_comments,
#                                      mail_author=mail_author)
#
#     return HttpResponseRedirect('blog/post/'+str(post_id)+'/', {'form': form})


@login_required
def add_comment(request, post_id):
    if request.method == 'POST':
        form = CommentsForm(request.POST or None)
        if form.is_valid():
            comment = form.save(commit=False)
            text_comments = form.cleaned_data.get('text_comments')
            author_comments = form.cleaned_data.get('author_comments')
            mail_author = form.cleaned_data.get('mail_author')


            comment.post = PoSt.objects.get(id=post_id)
            comment.post.objects.create(text_comments=text_comments, author_comments=author_comments,
                                        mail_author=mail_author)
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = CommentsForm()
    return render(request, 'post.html', {'form': form})