from django.contrib import admin
from perl.models import Latest_Projects, Latest_images, WorkExperience, SkIlls, \
    TestImonials, LanGuages, PoSt, Comments, PostImages, CategoryPost

admin.site.register(Latest_Projects)
admin.site.register(Latest_images)
admin.site.register(WorkExperience)
admin.site.register(SkIlls)
admin.site.register(TestImonials)
admin.site.register(LanGuages)
admin.site.register(PoSt)
admin.site.register(Comments)
admin.site.register(PostImages)
admin.site.register(CategoryPost)
