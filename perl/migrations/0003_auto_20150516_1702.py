# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0002_auto_20150516_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='comments_primary',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
