# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_category', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='ComMents',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mail_author', models.EmailField(max_length=50, null=True, blank=True)),
                ('text_comments', models.TextField()),
                ('date_comments', models.DateTimeField()),
                ('author_comments', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='LanGuages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(max_length=100)),
                ('level', models.CharField(max_length=70, choices=[(b'basic', b'basic'), (b'middle', b'middle'), (b'expert', b'expert')])),
                ('lang_progress', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'verbose_name': '\u0417\u043d\u0430\u043d\u0438\u0435 \u044f\u0437\u044b\u043a\u0430',
                'verbose_name_plural': '\u0417\u043d\u0430\u043d\u0438\u0435 \u044f\u0437\u044b\u043a\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='Latest_images',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('picture', models.ImageField(null=True, upload_to=b'small_img/', blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430',
                'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='Latest_Projects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250)),
                ('slug', models.SlugField()),
                ('main_image', models.ImageField(null=True, upload_to=b'big_img/', blank=True)),
                ('small_description', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('primary', models.BooleanField(default=b'primary')),
                ('date_created', models.DateTimeField()),
                ('date_updated', models.DateField()),
                ('images', models.ManyToManyField(to='perl.Latest_images', null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0435\u043a\u0442',
                'verbose_name_plural': '\u041f\u0440\u043e\u0435\u043a\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='PoSt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_post', models.CharField(max_length=200)),
                ('small_description_post', models.CharField(max_length=250)),
                ('description_post', models.TextField()),
                ('preview_image', models.ImageField(null=True, upload_to=b'post_preview_image/', blank=True)),
                ('date_created_post', models.DateField()),
                ('date_changed_post', models.DateTimeField()),
                ('comments_primary', models.BooleanField(default=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(to='perl.CategoryPost')),
            ],
        ),
        migrations.CreateModel(
            name='PostImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_post_images', models.CharField(max_length=50)),
                ('many_img', models.ImageField(null=True, upload_to=b'many_image_post/', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='SkIlls',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_language', models.CharField(max_length=100)),
                ('level', models.CharField(max_length=70, choices=[(b'basic', b'basic'), (b'middle', b'middle'), (b'expert', b'expert')])),
                ('progress', models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'verbose_name': '\u041d\u0430\u0432\u044b\u043a',
                'verbose_name_plural': '\u041d\u0430\u0432\u044b\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='TestImonials',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('response', models.CharField(max_length=250)),
                ('author_response', models.CharField(max_length=50)),
                ('company_response', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': '\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0430\u0446\u0438\u044f',
                'verbose_name_plural': '\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0430\u0446\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='WorkExperience',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_work', models.CharField(max_length=250)),
                ('company_name', models.CharField(max_length=250)),
                ('date_work', models.DateField()),
                ('date_end_work', models.DateField()),
                ('url_work', models.URLField(max_length=50)),
                ('description_work', models.TextField()),
            ],
            options={
                'verbose_name': '\u0420\u0430\u0431\u043e\u0442\u0430',
                'verbose_name_plural': '\u0420\u0430\u0431\u043e\u0442\u0430',
            },
        ),
        migrations.AddField(
            model_name='post',
            name='many_image_post',
            field=models.ManyToManyField(to='perl.PostImages', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='comments',
            name='post',
            field=models.ForeignKey(to='perl.PoSt'),
        ),
    ]
