# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0005_auto_20150504_1745'),
    ]

    operations = [
        migrations.CreateModel(
            name='LanguageS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(max_length=100)),
                ('level_language', models.CharField(max_length=100)),
                ('star', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='testimonials',
            name='response',
            field=models.CharField(max_length=250),
            preserve_default=True,
        ),
    ]
