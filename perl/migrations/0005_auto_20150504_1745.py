# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0004_auto_20150504_1736'),
    ]

    operations = [
        migrations.CreateModel(
            name='TestImonials',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('response', models.TextField()),
                ('author_response', models.CharField(max_length=50)),
                ('company_response', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='skills',
            name='author_response',
        ),
        migrations.RemoveField(
            model_name='skills',
            name='company_response',
        ),
        migrations.RemoveField(
            model_name='skills',
            name='date_skill',
        ),
        migrations.RemoveField(
            model_name='skills',
            name='description_skill',
        ),
        migrations.RemoveField(
            model_name='skills',
            name='response',
        ),
    ]
