# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0003_workexperience_description_work'),
    ]

    operations = [
        migrations.CreateModel(
            name='SkiLls',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description_skill', models.TextField(null=True, blank=True)),
                ('name_language', models.CharField(max_length=100)),
                ('level', models.CharField(max_length=70)),
                ('progress', models.IntegerField()),
                ('date_skill', models.DateField()),
                ('response', models.TextField()),
                ('author_response', models.CharField(max_length=50)),
                ('company_response', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='latest_projects',
            name='main_image',
            field=models.ImageField(null=True, upload_to=b'big_img/', blank=True),
            preserve_default=True,
        ),
    ]
