# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postimages',
            name='many_img',
            field=models.ImageField(default=1, upload_to=b'many_image_post/'),
            preserve_default=False,
        ),
    ]
