# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0002_remove_workexperience_description_work'),
    ]

    operations = [
        migrations.AddField(
            model_name='workexperience',
            name='description_work',
            field=models.TextField(default=2),
            preserve_default=False,
        ),
    ]
