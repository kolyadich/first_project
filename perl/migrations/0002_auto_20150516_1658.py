# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('perl', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComMents',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('author_comments', models.CharField(max_length=200)),
                ('mail_author', models.URLField(max_length=50, null=True, blank=True)),
                ('text_comments', models.TextField()),
                ('date_comments', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PoSt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_post', models.CharField(max_length=200)),
                ('small_description_post', models.CharField(max_length=200)),
                ('description_post', models.TextField()),
                ('preview_image', models.ImageField(null=True, upload_to=b'post_preview_image/', blank=True)),
                ('date_created_post', models.DateTimeField()),
                ('date_changed_post', models.DateTimeField()),
                ('category', models.CharField(max_length=100)),
                ('author', models.CharField(max_length=200)),
                ('comments_primary', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PostImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_post_images', models.CharField(max_length=50)),
                ('many_img', models.ImageField(null=True, upload_to=b'many_image_post/', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='post',
            name='many_image_post',
            field=models.ManyToManyField(to='perl.PostImages'),
            preserve_default=True,
        ),
    ]
