from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from perl import views

urlpatterns = patterns('',
    # Examples:

    url(r'^', include('logines.urls')),
    url(r'^$', views.ind, name='ind'),
    url(r'^blog/$', views.blog_paginator, name='blog_paginator'),
    url(r'^blog/post/(?P<post_id>\d+)/$', views.one_post, name='one_post'),
    url(r'^blog/post/(?P<post_id>\d+)/$', views.add_comment, name='add_comment'),
    url(r'^latest-project/(?P<slug>\w+)/$', views.view_latest_project, name='view_latest_project'),
    url(r'^admin/', include(admin.site.urls)),

)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
)